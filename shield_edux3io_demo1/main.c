/**
 * Shield-EDUx3IO - Testing scripts.
 * @author Neven Boyanov
 * This is part of the Tinusaur/Shield-EDUx3IO project.
 * Also used in Blocktinu Tools as demo andtesting script.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2023 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/shield-edux3io
 */

// ============================================================================

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "servolibtiny/servolibtiny.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~ Shield-EDUx3IO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//              Shield EDUx3IO
//                  +-------+
//      (RST)-->    +   Vcc +---(+)--VCC--
// ----BUTTON--> +--+   PB2 +---PHOTORES--
// -----BUZZER---+ PB4  PB1 +-------
// --------(-)---+ GND  PB0 +---LED------- (red/white)
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ----------------------------------------------------------------------------
// #include "shield_edux3io/shield_edux3io.h"
// NOTE: Some of the file content copied below.

// Ports definitions

#define SHIELD_EDUX3IO_LED PB0	// Define the LED (red/white) I/O port
#define SHIELD_EDUX3IO_BUTTON PB3	// Define the BUTTON I/O port
#define SHIELD_EDUX3IO_PHOTORES PB2	// Define the PHOTORES I/O port
// Define the ADMUX value to be used for the PHOTORES
// MUX[3:0] - 0000=ADC0(PB5); 0001=ADC1(PB2); 0010=ADC2(PB4); 0011=ADC3(PB3)
#define SHIELD_EDUX3IO_PHOTORES_MUX (1 << MUX0)
#define SHIELD_EDUX3IO_BUZZER PB4	// Define the BUZZER I/O port

// ----------------------------------------------------------------------------

// Define the I/O port and params to be used for the SERVO.
#define SERVO_PORT PB1
#define SERVO_POS_MIN     0		// Should be   0	(min: -100)
#define SERVO_POS_MID   125		// Should be 125
#define SERVO_POS_MAX   250		// Should be 250	(max: 350)
#define SERVO_POS_STEP   40		// 
#define SERVO_PULSE_COUNT 5		//

// ----------------------------------------------------------------------------

#define BUZZ_TICK_DELAY 480

typedef union {
	uint16_t data16;
	struct {
		uint8_t lo;
		uint8_t hi;
	};
} shield_edux3io_photores_result;

int main(void) {

	// ---- Initialization ----
	servolibtiny_resel(0, SERVO_PORT);		// Select servo and init port.
	
	// Set ports as output.
	DDRB |= (1 << SHIELD_EDUX3IO_BUZZER);
	DDRB |= (1 << SHIELD_EDUX3IO_LED);
	// Set ports as input.
	DDRB &= ~(1 << SHIELD_EDUX3IO_BUTTON);	// Set the Button port as input
	PORTB |= (1 << SHIELD_EDUX3IO_BUTTON);	// Set the Button port pull-up resistor

	// Init the ADC for the Photoresistor analog input
	ADMUX =
		(0 << ADLAR) |	// Set left shift result - this SHOULD be 0
		(SHIELD_EDUX3IO_PHOTORES_MUX);	// Set ADCx input (on start conversions)
	ADCSRA =
		(1 << ADEN) |	// Enable ADC
		(1 << ADATE) |	// Enable auto trigger enable
		(1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);	// Prescaler: 111=7: 1/128th ... 1MHz / 128 = 7812Hz
	ADCSRB = 0;	// Set free running mode
	ADCSRA |= (1 << ADSC);	// Set start conversions
	_delay_ms(ADCX_CHANGE_DELAY); // Small delay may be necessary for correct first ADC reading.

	// ---- Main loop ----
	shield_edux3io_photores_result photores_result;
	int16_t servo_pos = SERVO_POS_MID;
	int8_t servo_step = SERVO_POS_STEP;
	
	for (;;) { // Infinite main loop
		photores_result.lo = ADCL;
		photores_result.hi = ADCH;
		uint16_t buzzer_delay = photores_result.data16 + 1;	// Add "+1" to avoid zero value.
		if (!(PINB & (1 << SHIELD_EDUX3IO_BUTTON))) { // Check the status of the button.
			buzzer_delay = (buzzer_delay >> 2) + 1;	// Divide the result, and "+1" to avoid zero value.
		}
		PORTB |= (1 << SHIELD_EDUX3IO_LED); // Turn the LED on.
		// Buzzer beep
		for (uint8_t buzz_len = 120; buzz_len > 0; buzz_len--) {
			PORTB |= (1 << SHIELD_EDUX3IO_BUZZER);
			_delay_loop_2(buzzer_delay);
			PORTB &= ~(1 << SHIELD_EDUX3IO_BUZZER);
			_delay_loop_2(buzzer_delay);
		}
		PORTB &= ~(1 << SHIELD_EDUX3IO_LED); // Turn the LED off.
		servolibtiny_pos(servo_pos, SERVO_PULSE_COUNT);	// Set servo in position
		if (servo_pos > SERVO_POS_MAX) servo_step = -SERVO_POS_STEP;
		if (servo_pos < SERVO_POS_MIN) servo_step = SERVO_POS_STEP;
		servo_pos += servo_step;
	}

	return 0; // Return the mandatory for the "main" function int value. It is "0" for success.
}

// ============================================================================
